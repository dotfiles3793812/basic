-- Neovim encoding
vim.o.encoding = 'utf-8'
-- Buffer encoding
vim.bo.fileencoding = 'utf-8'
-- Set bomb
--vim.cmd('scriptencoding utf-8')

vim.wo.wrap = false
vim.o.listchars = 'tab:» ,trail:~,extends:›,precedes:‹'
vim.o.list = true
vim.wo.number = true
vim.wo.relativenumber = true

-- Enable 24-bit colors
vim.o.termguicolors = true
vim.cmd('colorscheme habamax')
